﻿# ME-C3100 Computer Graphics, Fall 2014
# Lehtinen / Kemppinen, Seppälä
#
# Assignment 6: Real-Time Shading

Student name: Markus Teivo
Student number: 292627
Hours spent on requirements (approx.): 11
Hours spent on extra credit (approx.):

# First, a 10-second poll about this assignment period:

Did you go to exercise sessions?
No

Did you work on the assignment using Aalto computers, your own computers, or both?
Both

# Which parts of the assignment did you complete? Mark them 'done'.
# You can also mark non-completed parts as 'attempted' if you spent a fair amount of
# effort on them. If you do, explain the work you did in the problems/bugs section
# and leave your 'attempt' code in place (commented out if necessary) so we can see it.

R1       Sample diffuse texture (1p): done
R2        Sample normal texture (1p): done
R3  Blinn-Phong diffuse shading (2p): done
R4 Blinn-Phong specular shading (2p): done
R5     Normal transform insight (4p): done

# Did you do any extra credit work?
No

(Describe what you did and, if there was a substantial amount of work involved, how you did it. Also describe how to use/activate your extra features, if they are interactive.)

# Are there any known problems/bugs remaining in your code?
R5:ssä, specular shading ei toimi niin kuin olen selittänyt. Kuvan "kiilto" ei liiku kameran mukana samalla
tavalla kuin käytettäessä camera spacea. Koska positionVarying on jo valmiiksi camera spacessa, pitäisi se
saada jotenkin muunnettua world spaceen jotta specular toimisi oikein. En keksinyt miten se tehdään.

(Please provide a list of the problems. If possible, describe what you think the cause is, how you have attempted to diagnose or fix the problem, and how you would attempt to diagnose or fix it if you had more time or motivation. This is important: we are more likely to assign partial credit if you help us understand what's going on.)

# Did you collaborate with anyone in the class?
Mika Kosonen, Essi Jukkala, Ilkka Oksanen, Klaus Helenius, selvensivät tehtäviä ja pohdimme R5:tä.

(Did you help others? Did others help you? Let us know who you talked to, and what sort of help you gave or received.)

# Any other comments you'd like to share about the assignment or the course so far?
Tällä kierroksella oli hankala saada selvää, miltä lopputuloksen pitäisi näyttää tehtävissä.
Varsinkin specular shading -tehtävässä handoutista ei selvästi nähnyt millaista lopputulosta haetaan,
ja examplessa piti olla todella tarkkasilmäinen nähdäkseen mikä eroaa. Edestä päin katsottaessa välttämättä
ei huomannut mitään eroa vaikka oli tehnyt jotakin väärin.

(Was the assignment too long? Too hard? Fun or boring? Did you learn something, or was it a total waste of time? Can we do something differently to help you learn? Please be brutally honest; we won't take it personally.)