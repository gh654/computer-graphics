#include "integrators.hpp"

#include "utility.hpp"
#include "particle_systems.hpp"

void eulerStep(ParticleSystem& ps, float step) {
	// YOUR CODE HERE (R1)
	// Implement an Euler integrator.
	State currState = ps.state();
	State derivative = ps.evalF(currState);
	State newState;
	for (unsigned i = 0; i < currState.size(); i++) {
		newState.push_back(currState[i] + (step * derivative[i]));
	}
	ps.set_state(newState);
};

void trapezoidStep(ParticleSystem& ps, float step) {
	// YOUR CODE HERE (R3)
	// Implement a trapezoid integrator.
	State currState = ps.state(); // X
	State derivative_0 = ps.evalF(currState); // f_0
	State midState;
	State newState;
	for (unsigned i = 0; i < currState.size(); i++) {
		midState.push_back(currState[i] + (step * derivative_0[i]));
	}
	State derivative_1 = ps.evalF(midState); // f_1
	for (unsigned i = 0; i < currState.size(); i++) {
		newState.push_back(currState[i] + (step / 2) * (derivative_0[i] + derivative_1[i]));
	}
	ps.set_state(newState);
}

void midpointStep(ParticleSystem& ps, float step) {
	const auto& x0 = ps.state();
	auto n = x0.size();
	auto f0 = ps.evalF(x0);
	auto xm = State(n), x1 = State(n);
	for (auto i = 0u; i < n; ++i) {
		xm[i] = x0[i] + (0.5f * step) * f0[i];
	}
	auto fm = ps.evalF(xm);
	for (auto i = 0u; i < n; ++i) {
		x1[i] = x0[i] + step * fm[i];
	}
	ps.set_state(x1);
}

void rk4Step(ParticleSystem& ps, float step) {
	// EXTRA: Implement the RK4 Runge-Kutta integrator.
	State currState = ps.state(); // X
	State k1 = ps.evalF(currState);

	State state1;
	for (unsigned i = 0; i < currState.size(); i++) {
		state1.push_back(currState[i] + (step / 2) * k1[i]);
	}
	State k2 = ps.evalF(state1);

	State state2;
	for (unsigned i = 0; i < currState.size(); i++) {
		state2.push_back(currState[i] + (step / 2) * k2[i]);
	}
	State k3 = ps.evalF(state2);

	State state3;
	for (unsigned i = 0; i < currState.size(); i++) {
		state3.push_back(currState[i] + (step * k3[i]));
	}
	State k4 = ps.evalF(state3);

	State newState;
	for (unsigned i = 0; i < currState.size(); i++) {
		newState.push_back(currState[i] + (step / 6) * (k1[i] + 2.0f*k2[i] + 2.0f*k3[i] + k4[i]));
	}

	ps.set_state(newState);
}
 
