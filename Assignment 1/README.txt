﻿# ME-C3100 Computer Graphics, Fall 2014
# Lehtinen / Kemppinen, Seppälä
#
# Assignment 1: Introduction

Student name: Markus Teivo
Student number: 292627
Hours spent on requirements (approx.): 8
Hours spent on extra credit (approx.): 5

# First, some questions about where you come from and how you got started.
# Your answers in this section will be used to improve the course.
# They will not be judged or affect your points, but please answer all of them.
# Keep it short; answering shouldn't take you more than 5-10 minutes.

- What are you studying here at Aalto? (Department, major, minor...?)
Nyt tietotekniikkaa, vaihtanut sähköltä lukuvuodenvaihteessa

- Which year of your studies is this?
1. tietotekniikalla, 3. Aallossa

- Is ME-C3100 a mandatory course for you?
Kuuluu joukkoon josta on pakko valita tietyt kurssit

- Have you had something to do with graphics before? Other studies, personal interests, work?
Yksinkertaista 2D-grafiikkaa C ja C++ kursseilla, Blenderiä lukiossa, en voi kuitenkaan sanoa osaavani paljoakaan.

- Do you remember basic linear algebra? Matrix and vector multiplication, cross product, that sort of thing?
Suhteellisen hyvin

- How is your overall programming experience? What language are you most comfortable with?
C, C++ ja Python tuttuja, C++ on mukavin.

- Do you have some experience with these things? (If not, do you have experience with something similar such as C or Direct3D?)
C++: Kyllä
C++11: Kyllä
OpenGL: Ei mitään

- Have you used a version control system such as Git, Mercurial or Subversion? Which ones?
En

- Did you go to the technology lecture?
Yes

- Did you go to exercise sessions?
Yes

- Did you work on the assignment using Aalto computers, your own computers, or both?
Both

# Which parts of the assignment did you complete? Mark them 'done'.
# You can also mark non-completed parts as 'attempted' if you spent a fair amount of
# effort on them. If you do, explain the work you did in the problems/bugs section
# and leave your 'attempt' code in place (commented out if necessary) so we can see it.

(Try to get everything done! Based on previous data, virtually everyone who put in the work and did well in the first two assignments ended up finishing the course, and also reported a high level of satisfaction at the end of the course.)

                            opened this file (0p): done
                         R1 Moving an object (1p): done
R2 Generating a simple cone mesh and normals (3p): done
  R3 Converting mesh data for OpenGL viewing (3p): done
           R4 Loading a large mesh from file (3p): done

# Did you do any extra credit work?
Rotate and scale transforms, Transforming normals in vertex shader, Version control, Animation
Rotation: keys ',' and '.'. Scaling: keys + and -. Animation: R key.
Normals transformed with transpose(inverse(modelToWorld3))*aNormal;.
Scaling and rotation done by creating matrices for scales and rotation and then multiplying them to modelToWorld matrix.
Animation: key R starts and stops the timer, time between frames is multiplied with pi and then added to camera_rotation_angle_.
Version control done using Bitbucket and Git.

(Describe what you did and, if there was a substantial amount of work involved, how you did it. Also describe how to use/activate your extra features, if they are interactive.)

# Are there any known problems/bugs remaining in your code?

(Please provide a list of the problems. If possible, describe what you think the cause is, how you have attempted to diagnose or fix the problem, and how you would attempt to diagnose or fix it if you had more time or motivation. This is important: we are more likely to assign partial credit if you help us understand what's going on.)

# Did you collaborate with anyone in the class?
Mika Kosonen, helped him with Git and received help with shaders

(Did you help others? Did others help you? Let us know who you talked to, and what sort of help you gave or received.)

# Any other comments you'd like to share about the assignment or the course so far?
Kierroksesta pääsi lopulta hyvin kärryille, oli sopivan haastavat tehtävät, tosin medium- ja hard-tason tehtäviin ei ole tässä vaiheessa asiaa.

(Was the assignment too long? Too hard? Fun or boring? Did you learn something, or was it a total waste of time? Can we do something differently to help you learn? Please be brutally honest; we won't take it personally.)

