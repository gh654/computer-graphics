#include <iostream>
#include <vector>
#include <algorithm>
#include <cassert>

#include "someheader.hpp"

using namespace std;

class SomeClass {
public:
	SomeClass() {
		cout << "constructor" << endl;
	}
	~SomeClass() {
		cout << "destructor" << endl;
	}
	int x;
	int y;
private:
	// ...
};


int sum(int, int);
void add_one_to_argument(SomeStruct&);

void print_vec(const vector<int>& v) {
	//for (int i = 0; i < v.size(); ++i) {
	//	cout << v[i];
	//}
	for (int i : v) {
		cout << i;
	} 
	cout << endl;
}

int main() {
	vector<int> v = {8, 3, 2, 3};
	print_vec(v);
	sort(begin(v), end(v));
	print_vec(v);

	//int anotherz{12.12f}; // modern initializer syntax prevents automatic type conversion
	//int z = 12.12f; // these days, VS does warn you if you do this

	auto it = begin(v);
	cout << *it << endl;

	// can't copy construct: vector<double> and vector<int> are not related
	// vector<double> bad(v);
	// but assigning individual ints into doubles is fine
	// so can initialize a vector to copy an iterator range
	auto vd = vector<double>(begin(v), end(v));

	std::string s = "somestring";
	auto reversed = std::string(rbegin(s), rend(s));

	// Below this point: advanced stuff covered after the end of the lecture.
	// Totally not necessary for the course (but might prove useful sometimes).

	// Customizing algorithm logic with a lambda: very convenient.
	// This sorts our vector in reverse direction instead of normal direction. 
	sort(begin(v), end(v),
		[](int left, int right) { return left > right; });
	print_vec(v);

	// Lambda with capture list: can use variables from outside the lambda
	int outside = 99;
	auto some_function = [&](int x){ return x + outside; };
	cout << some_function(4) << endl;

	return 0;
}

void add_one_to_argument(SomeStruct& s) {
	s.x += 1;
}

int sum(int i, int j) {
	return i + j;
}
