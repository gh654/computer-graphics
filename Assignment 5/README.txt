﻿# ME-C3100 Computer Graphics, Fall 2014
# Lehtinen / Kemppinen, Seppälä
#
# Assignment 5: Ray Tracing

Student name: Markus Teivo
Student number: 292627
Hours spent on requirements (approx.): 17
Hours spent on extra credit (approx.):

# First, a 10-second poll about this assignment period:

Did you go to exercise sessions?
Yes

Did you work on the assignment using Aalto computers, your own computers, or both?
Both

# Which parts of the assignment did you complete? Mark them 'done'.
# You can also mark non-completed parts as 'attempted' if you spent a fair amount of
# effort on them. If you do, explain the work you did in the problems/bugs section
# and leave your 'attempt' code in place (commented out if necessary) so we can see it.

R1 Generating rays & ambient lighting (  1p): done
R2 Visualizing depth		      (0.5p): done
R3 Perspective camera		      (0.5p): done
R4 Phong shading		      (  3p): done
R5 Plane intersection		      (0.5p): done
R6 Triangle intersection	      (  1p): done
R7 Shadows			      (  1p): done
R8 Mirror reflection		      (  1p): done
R9 Antialiasing			      (1.5p): not done

# Did you do any extra credit work?
Nope

(Describe what you did and, if there was a substantial amount of work involved, how you did it. Also describe how to use/activate your extra features, if they are interactive.)

# Are there any known problems/bugs remaining in your code?
Nope

(Please provide a list of the problems. If possible, describe what you think the cause is, how you have attempted to diagnose or fix the problem, and how you would attempt to diagnose or fix it if you had more time or motivation. This is important: we are more likely to assign partial credit if you help us understand what's going on.)

# Did you collaborate with anyone in the class?
Mika Kosonen with triangle and reflection problems

(Did you help others? Did others help you? Let us know who you talked to, and what sort of help you gave or received.)

# Any other comments you'd like to share about the assignment or the course so far?
Assignment was a bit different from previous ones, debugging was different as could not use std::cout.
Some adaptations were needed.

(Was the assignment too long? Too hard? Fun or boring? Did you learn something, or was it a total waste of time? Can we do something differently to help you learn? Please be brutally honest; we won't take it personally.)

